package com.gitlab.linde9821;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.BasicConfigurator;

@Slf4j
public class TicTacToe {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        log.info("Started TicTacToe");
    }
}
